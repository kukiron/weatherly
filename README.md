# Weatherly

Browser extension with a beautiful interface to show weather condition at your current location. The browser popup has a Windows 10 lock screen inspired look.

The extension uses Dark Sky, HTML5 Geolocation API and Skycons icon library.
Now, weather map (provided by Dark Sky) is available with many detail features.

### Using the extension
To check out the repo, run the following (if you have Git installed) from terminal:
```
> git clone https://kukiron@bitbucket.org/kukiron/weatherly.git
> cd Weatherly
```

To use the extension, download the .zip file, extract it & then upload the extension folder in browser's developer mode. Or alternatively, you can download the .crx file & just drag it to the browser's (chrome or opera) extension page.
